//当前 白棋1 还是 黑棋2
let redOrBlack;
let redOrBlackFlag;
let audienceNum = false //人员已满  观众
//页面打开自动链接 http://localhost:3000 后端服务
let prot = 'http://localhost:3003' //
let mySocket = io(prot) //直接写后端服务地址
//一直在监听webEvent 这个事件  
mySocket.on('webEvent', (res) => {
    // window.alert(res)
    //打印连接成功信息
    console.log('res===', res);
    if (res.includes('人员已满')) {
        alert("该房间人员已满，只能观战")
        audienceNum = true
        return
    }
})
let roomID = window.prompt("请输入你要进入的房间号：")
mySocket.emit('roomID', roomID)
//等棋盘生成完再显示
setTimeout(() => {
    //观众不显示
    if (!audienceNum) {
        //输入要玩白棋还是黑棋
        redOrBlack = window.prompt("选择白棋请输入1，黑棋输入2")
        if (redOrBlack === '1') {
            redOrBlackFlag = 1
        } else {
            redOrBlackFlag = 2
        }
    }
}, 1000);
//websocket   清空页面
mySocket.on('getItemsArr', (getItemsArr) => {
    const ItemsArr = JSON.parse(getItemsArr)
    //重置白
    lineXAndlineYArrRed = ItemsArr.lineXAndlineYArrRed
    //重置黑
    lineXAndlineYArrBlack = ItemsArr.lineXAndlineYArrBlack
    //重置总数组
    hasChessmanArrList = ItemsArr.hasChessmanArrList
    blackOrRedChessman = ++ItemsArr.blackOrRedChessman
    // 判断这次点击棋子颜色
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    init()
    //生成棋子 放置棋盘中
    lineXAndlineYArrRed.map(i => {
        new Chessman(i[0], i[1], color1).update()
    })
    lineXAndlineYArrBlack.map(i => {
        new Chessman(i[0], i[1], color2).update()
    })
    //改变页面上的信息
    redP.innerHTML = '白旗已下' + lineXAndlineYArrRed.length
    blackP.innerHTML = '黑旗已下' + lineXAndlineYArrBlack.length
})
//websocket 判断胜利 
mySocket.on('clearItemsArrAll', (getItemsArr) => {
    const ItemsArr = JSON.parse(getItemsArr)
    lineXAndlineYArrRed = ItemsArr.lineXAndlineYArrRed
    lineXAndlineYArrBlack = ItemsArr.lineXAndlineYArrBlack
    hasChessmanArrList = ItemsArr.hasChessmanArrList
    blackOrRedChessman = ++ItemsArr.blackOrRedChessman
    //赢了
    resultP.innerHTML = blackOrRedChessman % 2 == 0 ? '黑棋子方胜利' : '白棋子方胜利'
    var r = window.confirm(resultP.innerHTML + "，是否重新开始!");
    if (r == true) {
        redP.innerHTML = '白旗已下0'
        blackP.innerHTML = '黑旗已下0'
        againInit()
    }
})
//生成画布
let canvas = document.getElementById('canvas')
//白方p
let redP = document.getElementsByClassName('redP')[0]
//黑方p
let blackP = document.getElementsByClassName('blackP')[0]
//结果p
let resultP = document.getElementsByClassName('resultP')[0]
//画笔
let ctx = canvas.getContext('2d')
//canvas宽高
canvas.height = canvas.width = '450'
//颜色1  颜色2
let color1 = '#fff'
let color2 = 'black'
//棋子方格大小
let flag = 50
//横线对象数组
let getXLineArr = []
//当前点击坐标
let newARR = []
//竖线对象数组
let getYLineArr = []
//偶数白色 基数 黑色
let blackOrRedChessman = 0
//所有棋子数组
let hasChessmanArrList = []
//坐标点数组
//所有坐标数组
let lineXAndlineYArr = []
//白旗坐标数组
let lineXAndlineYArrRed = []
//黑棋坐标数组
let lineXAndlineYArrBlack = []
//初始化函数
init()

//网格构造函数
//color线条颜色
//线条间隔
function init() {
    //线条构造函数
    //线条颜色 x y
    function Line(color, x, y) {
        this.color = color;
        this.x = x
        this.y = y
    }
    //画横线
    Line.prototype.getXLine = function () {
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(0, this.y);
        ctx.lineTo(this.x, this.y);
        ctx.strokeStyle = this.color;
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
    //画竖线
    Line.prototype.getYLine = function () {
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(this.x, 0);
        ctx.strokeStyle = this.color;
        ctx.lineTo(this.x, this.y);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
    //线条相隔大小 后面 num += flag
    let num = 0
    //线条随机颜色数组
    // let colorsList = ["#33B5E5", "#0099CC", "#AA66CC", "#9933CC", "#99CC00", "#669900", "#FFBB33", "#FF8800", "#FF4444", "#CC0000"]
    //循环画线
    for (let i = 0; i <= canvas.width; i += flag) {
        // let colorx = colorsList[Math.floor(Math.random() * 10)]
        // let colory = colorsList[Math.floor(Math.random() * 10)]
        let colorx, colory
        colorx = colory = 'black'
        getXLineArr.push(new Line(colorx, canvas.width, num))
        getYLineArr.push(new Line(colory, num, canvas.width))
        num += flag
    }
    //画横线
    getXLineArr.map(item => {
        item.getXLine()
    })
    //画竖线
    getYLineArr.map(item => {
        item.getYLine()
    })
}

// 收集所有的坐标的 
for (let i = 0; i <= canvas.width; i += flag) {
    for (let j = 0; j <= canvas.width; j += flag) {
        lineXAndlineYArr.push([i, j])
    }
}
//画布距离浏览器上册的距离
let MarginTopNum = 200;
//画布距离浏览器左册的距离
let MarginLeftNum = 300;
//棋子构造函数
//x 横坐标 
//y 纵坐标 
//color 棋子颜色坐标 
//x 横坐标 
function Chessman(x, y, color) {
    this.x = x
    this.y = y
    this.color = color
}
Chessman.prototype.update = function () {
    ctx.save();
    ctx.beginPath()
    ctx.fillStyle = this.color// 背景颜色为白色
    ctx.arc(this.x, this.y, 20, 0, Math.PI * 2, false)
    ctx.fill()
    ctx.closePath()
}
// 点击事件   放棋子 ？存在 ：不存在 ？放 ：提醒
canvas.addEventListener('click', function (e) {
    //观众 直接return
    if (audienceNum) return false;
    //当前点击的坐标  相对于canvas的坐标
    let arr = [e.clientX - MarginLeftNum, e.clientY - MarginTopNum]
    //当前点击坐标到 棋盘 0，0 的距离
    // let newArrLong = Math.sqrt((0 - arr[0]) * (0 - arr[0]) + (0 - arr[1]) * (0 - arr[1]))
    //25
    let newArrLong = flag / 2
    newARR = []
    //循环判断当前点击坐标到哪个棋盘坐标的距离最短，棋子放置到该坐标点
    lineXAndlineYArr.map((item, index) => {
        let newArrLongA = Math.sqrt((item[0] - arr[0]) * (item[0] - arr[0]) + (item[1] - arr[1]) * (item[1] - arr[1]))
        if (newArrLongA <= newArrLong) {
            newARR = item; newArrLong = newArrLongA
        }
    })
    if (!newARR.toString()) {
        return
    }
    //白
    if (redOrBlackFlag === 1) {
        if (blackOrRedChessman % 2 !== 0) {
            return false
        }
    } else {
        //黑
        if (blackOrRedChessman % 2 == 0) {
            return false
        }
    }
    console.log("%c当前点击棋子坐标：", "color:red;font-size:20px;", newARR)
    //判断该点是否已经有棋子
    let hasYesOrNo = JSON.stringify(hasChessmanArrList).includes(JSON.stringify(newARR))
    if (!hasYesOrNo) {
        //没有的话加入棋子数组
        hasChessmanArrList.push(newARR)
    }
    else {
        alert('已经有棋子')
        return
    }

    //相应颜色的棋子放入对应颜色棋子数组
    blackOrRedChessman % 2 == 0 ? lineXAndlineYArrRed.push(newARR) : lineXAndlineYArrBlack.push(newARR)
    const sendItemsArr = JSON.stringify({ lineXAndlineYArrRed, lineXAndlineYArrBlack, hasChessmanArrList, blackOrRedChessman })
    //给服务端同步数据
    mySocket.emit('sendItemsArr', sendItemsArr)
    //判断棋子数量是否已经大于5
    let newlineXAndlineYArr
    blackOrRedChessman % 2 == 0 ? newlineXAndlineYArr = JSON.parse(JSON.stringify(lineXAndlineYArrRed)) : newlineXAndlineYArr = JSON.parse(JSON.stringify(lineXAndlineYArrBlack))
    //改变下一次棋子颜色使用
    blackOrRedChessman++
    //判断棋子数量是否已经大于5  没到5 决对赢不了的 ~
    if (newlineXAndlineYArr.length >= 5) {
        //向右向左倾斜判断、横向纵向判断
        leftRightFangXiang()
        // //向右倾斜
        function leftRightFangXiang() {
            // 深拷贝一份数据
            let newlineXAndlineYArrR = JSON.parse(JSON.stringify(newlineXAndlineYArr))
            //右下方向
            //当它是第0棵棋子的时候 其他四颗
            // 0   [[ newARR[0]+flag, newARR[1]+50],[ newARR[0]+100, newARR[1]+100],[ newARR[0]+150, newARR[1]+150],[ newARR[0]+200, newARR[1]+200]]

            //1    [[ newARR[0]-50, newARR[1]-50],[ newARR[0]+50, newARR[1]+50],[ newARR[0]+100, newARR[1]+100],[ newARR[0]+150, newARR[1]+150]]
            //左下方向
            //当它是第0棵棋子的时候 其他四颗
            // 0   [[ newARR[0]-50, newARR[1]+50],[ newARR[0]-100, newARR[1]+100],[ newARR[0]-150, newARR[1]+150],[ newARR[0]-200, newARR[1]+200]]

            //1    [[ newARR[0]+50, newARR[1]-50],[ newARR[0]-50, newARR[1]+50],[ newARR[0]-100, newARR[1]+100],[ newARR[0]-150, newARR[1]+150]]

            // // x方向  注释  y方向类似
            // 0[[newARR[0] + 50, newARR[1]], [newARR[0] + 100, newARR[1]], [newARR[0] + 150, newARR[1]], [newARR[0] + 200, newARR[1]]]

            // 1[[newARR[0] - 50, newARR[1]], [newARR[0] + 50, newARR[1]], [newARR[0] + 100, newARR[1]], [newARR[0] + 150, newARR[1]]]

            let newRightArrList = []
            for (let i = 0; i < 5; i++) {
                //x
                if (1) {
                    newRightArrList = []
                    for (let j = 0; j < i; j++) {
                        newRightArrList.push([newARR[0] - flag * (j + 1), newARR[1]])
                    }
                    for (let k = i; k < 4; k++) {
                        newRightArrList.push([newARR[0] + flag * (k - i + 1), newARR[1]])
                    }
                    if (rightOrRight(newRightArrList, newlineXAndlineYArrR, newARR) == 'win') {
                        console.log("%c横向赢了哦~", "color:red;font-size:30px;");
                        break
                    }
                }
                //y
                if (1) {
                    newRightArrList = []
                    for (let j = 0; j < i; j++) {
                        newRightArrList.push([newARR[0], newARR[1] - flag * (j + 1)])
                    }
                    for (let k = i; k < 4; k++) {
                        newRightArrList.push([newARR[0], newARR[1] + flag * (k - i + 1)])
                    }
                    if (rightOrRight(newRightArrList, newlineXAndlineYArrR, newARR) == 'win') {
                        console.log("%c纵向赢了哦~", "color:red;font-size:30px;");
                        break
                    }
                }
                // //左下   //  + -     张俊卿    - +
                if (1) {
                    newRightArrList = []
                    for (let j = 0; j < i; j++) {
                        newRightArrList.push([newARR[0] + flag * (j + 1), newARR[1] - flag * (j + 1)])
                    }
                    for (let k = i; k < 4; k++) {
                        newRightArrList.push([newARR[0] - flag * (k - i + 1), newARR[1] + flag * (k - i + 1)])
                    }
                    if (rightOrRight(newRightArrList, newlineXAndlineYArrR, newARR) == 'win') {
                        console.log("%c左下赢了哦~", "color:red;font-size:30px;");
                        break
                    }
                }
                //右下   //  - -     张俊卿    + +
                if (1) {
                    newRightArrList = []
                    for (let j = 0; j < i; j++) {
                        newRightArrList.push([newARR[0] - flag * (j + 1), newARR[1] - flag * (j + 1)])
                    }
                    for (let k = i; k < 4; k++) {
                        newRightArrList.push([newARR[0] + flag * (k - i + 1), newARR[1] + flag * (k - i + 1)])
                    }
                    if (rightOrRight(newRightArrList, newlineXAndlineYArrR, newARR) == 'win') {
                        console.log("%c右下赢了哦~", "color:red;font-size:30px;");
                        break
                    }
                }
            }
        }
    }
})
// 判断是否胜利
function rightOrRight(newRightArrList, newlineXAndlineYArrR, newARR) {
    newRightArrList = newRightArrList.map(item => JSON.stringify(item))
    newlineXAndlineYArrR = newlineXAndlineYArrR.map(item => JSON.stringify(item))
    let filterLIst = []
    newlineXAndlineYArrR.map(item => {
        newRightArrList.map(itemN => {
            if (itemN == item) {
                filterLIst.push(item)
            }
        })
    })
    if (filterLIst.length == 4) {
        if (youWinFun()) {
            return 'win'
        }
    }
}
//您赢了
function youWinFun() {
    //初始化函数
    mySocket.emit('clearItemsArr')
    return 'win'
}
//重新开始Fun
function againInit() {
    // window.history.go(0)
    //横线对象数组
    getXLineArr = []
    //竖线对象数组
    getYLineArr = []
    //偶数白色 基数 黑色
    blackOrRedChessman = 0
    //所有棋子数组
    hasChessmanArrList = []
    //坐标点数组
    //白旗坐标数组
    lineXAndlineYArrRed = []
    //黑棋坐标数组
    lineXAndlineYArrBlack = []
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    init()
}
